"use client";
import styles from "./page.module.css";
import usePeopleStore from "@/app/comparer/zustand/peopleStore";
import Link from "next/link";

export default function Home() {
  const { currentComparison, setCurrentComparison } = usePeopleStore();
  if (!currentComparison) setCurrentComparison();
  return (
    <main className={styles.main}>
      <p>Which mode do you want?</p>
      <Link href={"/comparer"}>Direkter Vergleich</Link>
    </main>
  );
}
