import styles from "@/app/comparer/page.module.css";
import { FC, useEffect } from "react";
import usePeopleStore from "@/app/comparer/zustand/peopleStore";

const Ranking: FC = () => {
  const people = usePeopleStore((state) => state.people);
  const toggle = usePeopleStore((state) => state.toggleShowRanking);

  return (
    <div className={styles.infoCardLeft}>
      Current Rankings:{" "}
      {people
        .sort((a, b) => b.comparerScore - a.comparerScore)
        .map((person, index) => (
          <div key={person.name}>
            {index + 1}. {person.name} {person.comparerScore}
          </div>
        ))}
      <button onClick={toggle}>x</button>
    </div>
  );
};

export default Ranking;
