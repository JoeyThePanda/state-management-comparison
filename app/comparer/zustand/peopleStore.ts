"use client";
import { create } from "zustand";
import { persist } from "zustand/middleware";
import { PEOPLE, Person } from "@/app/people";
import { getNewComparison } from "@/app/comparer/getNewComparison";
import resolveComparisonUtil from "@/app/comparer/resolveComparisonUtil";
import { immer } from "zustand/middleware/immer";

interface PeopleStoreType {
  people: Array<Person>;
  setCurrentComparison: () => void;
  currentComparison: [Person, Person];
  isEndAnimation: boolean;
  executeFightAnimation: () => Promise<void>;
  resolveComparison: ({
    winner,
    loser,
    isTie,
  }: {
    winner: Person;
    loser: Person;
    isTie: boolean;
  }) => void;
  showRanking: boolean;
  toggleShowRanking: () => void;
}

const usePeopleStore = create<PeopleStoreType>()(
  persist(
    immer((set, getState) => ({
      people: PEOPLE.map((person) => ({
        ...person,
        comparerScore: 0,
        matchUps: [],
      })),
      setCurrentComparison: () => {
        set((state) => {
          const currentComparison: [Person, Person] = getNewComparison(
            state.people,
          );
          while (currentComparison[0].name === currentComparison[1].name) {
            currentComparison[1] =
              state.people[Math.floor(Math.random() * state.people.length)];
          }
          state.currentComparison = currentComparison;
        });
      },
      currentComparison: [
        {
          ...PEOPLE[-1],
          matchUps: [],
          comparerScore: 0,
        },
        {
          ...PEOPLE[-1],
          matchUps: [],
          comparerScore: 0,
        },
      ],
      resolveComparison: (args) =>
        set((state) => {
          state.people = resolveComparisonUtil({
            people: state.people,
            ...args,
          });
        }),
      isEndAnimation: false,
      executeFightAnimation: async () => {
        getState().isEndAnimation = true;
        await new Promise((r) => setTimeout(r, 4000));
        getState().isEndAnimation = false;
      },
      showRanking: false,
      toggleShowRanking: () => {
        getState().showRanking = !getState().showRanking;
      },
    })),
    {
      name: "NerdOffCache",
      partialize: (state) => ({
        people: state.people,
      }),
    },
  ),
);

export default usePeopleStore;
