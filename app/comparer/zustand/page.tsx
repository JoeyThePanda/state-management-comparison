"use client";
import VersusMiddleColumn from "@/app/comparer/zustand/VersusMiddleColumn";
import PersonCard from "@/app/comparer/zustand/PersonCard/PersonCard";
import Ranking from "@/app/comparer/zustand/Ranking";
import styles from "@/app/comparer/page.module.css";
import useZustandComparer from "@/app/comparer/zustand/usePage";

export default function ZustandComparer() {
  const {
    showRanking,
    toggleShowRanking,
    currentComparison,
    resetButtonOnClick,
  } = useZustandComparer();

  return (
    <main className={styles.main}>
      <h3>Für wen stimmst du?</h3>
      {currentComparison && (
        <div className={styles.versus}>
          <PersonCard side={"left"} />
          <VersusMiddleColumn />
          <PersonCard side={"right"} />
        </div>
      )}
      <div>
        <button onClick={toggleShowRanking}>Rankings</button>
        <button onClick={resetButtonOnClick}>RESET</button>
      </div>
      {showRanking && <Ranking />}
    </main>
  );
}
