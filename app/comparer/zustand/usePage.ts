import { useRouter } from "next/navigation";
import usePeopleStore from "@/app/comparer/zustand/peopleStore";
import { useEffect } from "react";

const useZustandComparer = () => {
  const router = useRouter();
  const {
    currentComparison,
    setCurrentComparison,
    showRanking,
    toggleShowRanking,
  } = usePeopleStore();
  const resetButtonOnClick = () => {
    usePeopleStore.persist.clearStorage();
    router.replace("/comparer/zustand");
  };
  useEffect(() => {
    setCurrentComparison();
  }, [setCurrentComparison]);

  return {
    showRanking,
    toggleShowRanking,
    currentComparison,
    resetButtonOnClick,
  };
};

export default useZustandComparer;
