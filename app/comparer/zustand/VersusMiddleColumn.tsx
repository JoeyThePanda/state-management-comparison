import Image from "next/image";
import VSImage from "@/public/VS.png";
import styles from "@/app/comparer/page.module.css";
import usePeopleStore from "@/app/comparer/zustand/peopleStore";
import { useEffect } from "react";

const VersusMiddleColumn = () => {
  const { currentComparison, setCurrentComparison, resolveComparison } =
    usePeopleStore(
      ({ currentComparison, setCurrentComparison, resolveComparison }) => ({
        currentComparison,
        setCurrentComparison,
        resolveComparison,
      }),
    );

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image src={VSImage} alt={"VS"} width={80} />
      {currentComparison && (
        <button
          className={styles.tieButton}
          onClick={() => {
            if (
              currentComparison[0].comparerScore >
              currentComparison[1].comparerScore
            ) {
              resolveComparison({
                winner: currentComparison[0],
                loser: currentComparison[1],
                isTie: true,
              });
            } else {
              resolveComparison({
                winner: currentComparison[1],
                loser: currentComparison[0],
                isTie: true,
              });
            }
            setCurrentComparison();
          }}
        >
          Unentschieden
        </button>
      )}
    </div>
  );
};

export default VersusMiddleColumn;
