import Image from "next/image";
import VSImage from "@/public/VS.png";
import styles from "@/app/comparer/page.module.css";
import resolveComparisonUtil from "@/app/comparer/resolveComparisonUtil";
import { Person } from "@/app/people";
import { Dispatch, FC, SetStateAction } from "react";

interface VersusMiddleColumnProps {
  currentComparison: [Person, Person];
  setCurrentComparison: () => void;
  people: Array<Person>;
}

const VersusMiddleColumn: FC<VersusMiddleColumnProps> = ({
  currentComparison,
  setCurrentComparison,
  people,
}) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image src={VSImage} alt={"VS"} width={80} />
      {currentComparison && (
        <button
          className={styles.tieButton}
          onClick={() => {
            if (
              currentComparison[0].comparerScore >
              currentComparison[1].comparerScore
            ) {
              resolveComparisonUtil({
                winner: currentComparison[0],
                loser: currentComparison[1],
                isTie: true,
                people,
              });
            } else {
              resolveComparisonUtil({
                winner: currentComparison[1],
                loser: currentComparison[0],
                isTie: true,
                people,
              });
            }
            setCurrentComparison();
          }}
        >
          Unentschieden
        </button>
      )}
    </div>
  );
};

export default VersusMiddleColumn;
