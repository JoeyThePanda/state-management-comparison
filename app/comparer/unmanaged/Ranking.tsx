import styles from "@/app/comparer/page.module.css";
import { Dispatch, FC, SetStateAction } from "react";
import { Person } from "@/app/people";

interface InfoCardProps {
  isShown: boolean;
  setIsShown: Dispatch<SetStateAction<boolean>>;
  people: Array<Person>
}

const Ranking: FC<InfoCardProps> = ({ isShown, setIsShown, people }) => {
  return (
    isShown && (
      <div className={styles.infoCardLeft}>
        Current Rankings:{" "}
        {people
          .sort((a, b) => b.comparerScore - a.comparerScore)
          .map((person, index) => (
            <div key={person.name}>
              {index + 1}. {person.name} {person.comparerScore}
            </div>
          ))}
        <button onClick={() => setIsShown(false)}>x</button>
      </div>
    )
  );
};

export default Ranking;