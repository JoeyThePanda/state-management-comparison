import { useCallback, useEffect, useState } from "react";
import { PEOPLE, Person } from "@/app/people";
import { useRouter } from "next/navigation";
import { getNewComparison } from "@/app/comparer/getNewComparison";

const useUnmanagedComparer = () => {
  const [people, setPeople] = useState<Array<Person>>(
    PEOPLE.map((person) => ({
      ...person,
      comparerScore: 0,
      matchUps: [],
    })),
  );

  const [currentComparison, setCurrentComparison] = useState<[Person, Person]>([
    people[-1],
    people[-1],
  ]);
  const [isRankingShown, setIsRankingShown] = useState(false);
  const [isEndAnimation, setIsEndAnimation] = useState(false);
  const router = useRouter();
  const resetButtonOnClick = () => router.replace("/comparer/unmanaged");
  const executeFightAnimation = async () => {
    setIsEndAnimation(true);
    await new Promise((r) => setTimeout(r, 4000));
    setIsEndAnimation(false);
  };
  const setNewComparison = useCallback(() => {
    const newComparison: [Person, Person] = getNewComparison(people);
    while (newComparison[0].name === newComparison[1].name) {
      newComparison[1] = people[Math.floor(Math.random() * people.length)];
    }
    setCurrentComparison(newComparison);
  }, [people]);
  useEffect(() => {
    setNewComparison();
  }, [setNewComparison]);

  return {
    isRankingShown,
    setIsRankingShown,
    currentComparison,
    resetButtonOnClick,
    isEndAnimation,
    people,
    executeFightAnimation,
    setNewComparison,
    setPeople,
  };
};

export default useUnmanagedComparer;
