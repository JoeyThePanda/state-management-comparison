"use client";
import VersusMiddleColumn from "@/app/comparer/unmanaged/VersusMiddleColumn";
import PersonCard from "@/app/comparer/unmanaged/PersonCard/PersonCard";
import Ranking from "@/app/comparer/unmanaged/Ranking";
import styles from "@/app/comparer/page.module.css";
import useUnmanagedComparer from "@/app/comparer/unmanaged/usePage";


export default function UnmanagedComparer() {
  const {
    currentComparison,
    resetButtonOnClick,
    setIsRankingShown,
    isRankingShown,
    isEndAnimation,
    people,
    executeFightAnimation,
    setNewComparison,
    setPeople,
  } = useUnmanagedComparer();
  return (
    <main className={styles.main}>
      <h3>Für wen stimmst du?</h3>
      {currentComparison && (
        <div className={styles.versus}>
          <PersonCard
            side={"left"}
            isEndAnimation={isEndAnimation}
            currentComparison={currentComparison}
            people={people}
            executeFightAnimation={executeFightAnimation}
            setCurrentComparison={setNewComparison}
            setPeople={setPeople}
          />
          <VersusMiddleColumn
            people={people}
            currentComparison={currentComparison}
            setCurrentComparison={setNewComparison}
          />
          <PersonCard
            side={"right"}
            isEndAnimation={isEndAnimation}
            currentComparison={currentComparison}
            people={people}
            executeFightAnimation={executeFightAnimation}
            setCurrentComparison={setNewComparison}
            setPeople={setPeople}
          />
        </div>
      )}
      <div>
        <button onClick={() => setIsRankingShown(true)}>Rankings</button>
        <button onClick={resetButtonOnClick}>RESET</button>
      </div>
      <Ranking
        isShown={isRankingShown}
        setIsShown={setIsRankingShown}
        people={people}
      />
    </main>
  );
}
