"use client";
import { Dispatch, FC, SetStateAction } from "react";
import Image from "next/image";
import styles from "@/app/comparer/page.module.css";
import { clsx } from "clsx";
import { Person } from "@/app/people";
import { useUnmanagedPersonCard } from "@/app/comparer/unmanaged/PersonCard/useUnmanagedPersonCard";

interface PersonCardProps {
  side: "left" | "right";
  currentComparison: [Person, Person];
  isEndAnimation: boolean;
  people: Array<Person>;
  executeFightAnimation: () => Promise<void>;
  setCurrentComparison: () => void;
  setPeople: Dispatch<SetStateAction<Array<Person>>>;
}

const PersonCard: FC<PersonCardProps> = ({
  side,
  currentComparison,
  isEndAnimation,
  people,
  executeFightAnimation,
  setCurrentComparison,
  setPeople,
}) => {
  const {
    isDescriptionShown,
    setIsDescriptionShown,
    person,
    onWinningClick,
    getAnimationClass,
  } = useUnmanagedPersonCard(
    { side, currentComparison, isEndAnimation, people },
    executeFightAnimation,
    setCurrentComparison,
    setPeople,
  );
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {person && (
        <>
          <div
            onClick={onWinningClick}
            className={clsx(styles.clickable, getAnimationClass())}
          >
            <h2>{person.name}</h2>
            <p>Current Score {person.comparerScore}</p>
            {person.image && (
              <div className={styles.imageContainer}>
                <Image
                  src={person.image}
                  alt={person.name}
                  fill
                  style={{ objectFit: "contain" }}
                  sizes={"100%"}
                />
              </div>
            )}
          </div>
          <button onClick={() => setIsDescriptionShown(true)}>Mehr Info</button>
          {isDescriptionShown && (
            <div
              className={clsx(
                side === "left" ? styles.infoCardLeft : styles.infoCardRight,
              )}
            >
              <p>{person.description}</p>
              <button onClick={() => setIsDescriptionShown(false)}>X</button>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default PersonCard;
