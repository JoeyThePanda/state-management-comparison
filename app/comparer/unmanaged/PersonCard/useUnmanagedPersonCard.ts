"use client";
import usePeopleStore from "@/app/comparer/zustand/peopleStore";
import { Dispatch, SetStateAction, useState } from "react";
import styles from "@/app/comparer/page.module.css";
import { Person } from "@/app/people";
import resolveComparisonUtil from "@/app/comparer/resolveComparisonUtil";
import { getNewComparison } from "@/app/comparer/getNewComparison";

export const useUnmanagedPersonCard = (
  {
    side,
    isEndAnimation,
    currentComparison,
    people,
  }: {
    side: "left" | "right";
    isEndAnimation: boolean;
    currentComparison: [Person, Person];
    people: Array<Person>;
  },
  executeFightAnimation: () => Promise<void>,
  setCurrentComparison: Dispatch<SetStateAction<[Person, Person]>>,
  setPeople: Dispatch<SetStateAction<Array<Person>>>,
) => {
  const [isDescriptionShown, setIsDescriptionShown] = useState(false);
  const [isWinner, setIsWinner] = useState(false);
  const getAnimationClass = () => {
    if (isWinner && isEndAnimation && side === "left") return styles.winnerLeft;
    if (isWinner && isEndAnimation && side === "right")
      return styles.winnerRight;
    if (!isWinner && isEndAnimation && side === "left") return styles.loserLeft;
    if (!isWinner && isEndAnimation && side === "right")
      return styles.loserRight;
  };
  const [person, opponent] =
    side === "left"
      ? currentComparison
      : [currentComparison[1], currentComparison[0]];
  const onWinningClick = async () => {
    setPeople(
      resolveComparisonUtil({
        winner: person,
        loser: opponent,
        isTie: false,
        people,
      }),
    );
    setIsWinner(true);
    await executeFightAnimation();
    setIsWinner(false);

    const newComparison: [Person, Person] = getNewComparison(people);
    while (newComparison[0].name === newComparison[1].name) {
      newComparison[1] = people[Math.floor(Math.random() * people.length)];
    }
    setCurrentComparison(newComparison);
  };

  return {
    isDescriptionShown,
    setIsDescriptionShown,
    person,
    onWinningClick,
    getAnimationClass,
  };
};
