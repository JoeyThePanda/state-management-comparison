import { Person } from "@/app/people";

export const getNewComparison = (arr: Person[]): [Person, Person] => {
  // Only do this half the time
  if (Math.round(Math.random())) {
    return [
      arr[Math.floor(Math.random() * arr.length)],
      arr[Math.floor(Math.random() * arr.length)],
    ];
  }
  const valueIndicesMap = new Map<number, number[]>();

  for (let i = 0; i < arr.length; i++) {
    const obj = arr[i];
    const value = obj.comparerScore;

    if (!valueIndicesMap.has(value)) {
      valueIndicesMap.set(value, []);
    }
    valueIndicesMap.get(value)!.push(i);
  }
  const valuesWithMultipleIndices = Array.from(valueIndicesMap).filter(
    ([_, indices]) => {
      return indices.length > 1;
    },
  );

  if (valuesWithMultipleIndices.length === 0) {
    return [
      arr[Math.floor(Math.random() * arr.length)],
      arr[Math.floor(Math.random() * arr.length)],
    ];
  }

  const [_, indices] = valuesWithMultipleIndices.sort(
    (a, b) => b[1].length - a[1].length,
  )[0];
  return [
    arr[indices[Math.floor(Math.random() * indices.length)]],
    arr[indices[Math.floor(Math.random() * indices.length)]],
  ];
};
