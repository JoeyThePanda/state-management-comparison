import { useState } from "react";

export const useAnimation = () => {
  const [isEndAnimation, setIsEndAnimation] = useState(false);
  const executeFightAnimation = async () => {
    setIsEndAnimation(true);
    await new Promise((r) => setTimeout(r, 4000));
    setIsEndAnimation(false);
  };
  return {isEndAnimation, executeFightAnimation}
};