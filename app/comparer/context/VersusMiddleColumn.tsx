import Image from "next/image";
import VSImage from "@/public/VS.png";
import styles from "@/app/comparer/page.module.css";
import { useContext, useEffect } from "react";
import { ComparerContext } from "@/app/comparer/context/ComparerContext";

const VersusMiddleColumn = () => {
  const { currentComparison, setCurrentComparison, resolveComparison } =
    useContext(ComparerContext);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image src={VSImage} alt={"VS"} width={80} />
      {currentComparison && (
        <button
          className={styles.tieButton}
          onClick={() => {
            if (
              currentComparison[0].comparerScore >
              currentComparison[1].comparerScore
            ) {
              resolveComparison({
                winner: currentComparison[0],
                loser: currentComparison[1],
                isTie: true,
              });
            } else {
              resolveComparison({
                winner: currentComparison[1],
                loser: currentComparison[0],
                isTie: true,
              });
            }
            setCurrentComparison();
          }}
        >
          Unentschieden
        </button>
      )}
    </div>
  );
};

export default VersusMiddleColumn;
