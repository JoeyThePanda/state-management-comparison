import styles from "@/app/comparer/page.module.css";
import { FC, useContext } from "react";
import { ComparerContext } from "@/app/comparer/context/ComparerContext";

const Ranking: FC = () => {
  const { people, toggleRanking } = useContext(ComparerContext);

  return (
    <div className={styles.infoCardLeft}>
      Current Rankings:{" "}
      {people
        .sort((a, b) => b.comparerScore - a.comparerScore)
        .map((person, index) => (
          <div key={person.name}>
            {index + 1}. {person.name} {person.comparerScore}
          </div>
        ))}
      <button onClick={toggleRanking}>x</button>
    </div>
  );
};

export default Ranking;
