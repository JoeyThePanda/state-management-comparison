"use client";
import styles from "@/app/comparer/page.module.css";
import ComparerContextProvider, {
} from "@/app/comparer/context/ComparerContext";
import Content from "@/app/comparer/context/Content";

export default function ContextComparer() {
  return (
    <main className={styles.main}>
      <ComparerContextProvider>
        <Content />
      </ComparerContextProvider>
    </main>
  );
}
