import styles from "@/app/comparer/page.module.css";
import PersonCard from "@/app/comparer/context/PersonCard/PersonCard";
import VersusMiddleColumn from "@/app/comparer/context/VersusMiddleColumn";
import Ranking from "@/app/comparer/context/Ranking";
import useContextComparer from "@/app/comparer/context/usePage";

const Content = () => {
  const { toggleRanking, showRanking, currentComparison, resetButtonOnClick } =
    useContextComparer();
  return (
    <>
      <h3>Für wen stimmst du?</h3>
      {currentComparison && (
        <div className={styles.versus}>
          <PersonCard side={"left"} />
          <VersusMiddleColumn />
          <PersonCard side={"right"} />
        </div>
      )}
      <div>
        <button onClick={toggleRanking}>Rankings</button>
        <button onClick={resetButtonOnClick}>RESET</button>
      </div>
      {showRanking && <Ranking />}
    </>
  );
};

export default Content;
