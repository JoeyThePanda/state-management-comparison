import { useRouter } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import { ComparerContext } from "@/app/comparer/context/ComparerContext";

const useContextComparer = () => {
  const router = useRouter();
  const { currentComparison, setCurrentComparison, showRanking, toggleRanking } =
    useContext(ComparerContext);
  const resetButtonOnClick = () => {
    router.replace("/comparer/context");
  };
  useEffect(() => {
    setCurrentComparison();
  }, [setCurrentComparison]);

  return {
    showRanking,
    toggleRanking,
    currentComparison,
    resetButtonOnClick,
  };
};

export default useContextComparer;
