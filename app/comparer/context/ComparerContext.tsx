import { createContext, FC, ReactNode, useCallback, useState } from "react";
import { PEOPLE, Person } from "@/app/people";
import { getNewComparison } from "@/app/comparer/getNewComparison";
import resolveComparisonUtil from "@/app/comparer/resolveComparisonUtil";

interface ComparerContextType {
  people: Array<Person>;
  setCurrentComparison: () => void;
  currentComparison: [Person, Person];
  isEndAnimation: boolean;
  executeFightAnimation: () => Promise<void>;
  resolveComparison: ({
    winner,
    loser,
    isTie,
  }: {
    winner: Person;
    loser: Person;
    isTie: boolean;
  }) => void;
  showRanking: boolean;
  toggleRanking: () => void;
}

const defaultValue: ComparerContextType = {
  people: PEOPLE.map((person) => ({
    ...person,
    comparerScore: 0,
    matchUps: [],
  })),
  setCurrentComparison: () => {},
  currentComparison: [
    {
      ...PEOPLE[-1],
      matchUps: [],
      comparerScore: 0,
    },
    {
      ...PEOPLE[-1],
      matchUps: [],
      comparerScore: 0,
    },
  ],
  resolveComparison: () => {},
  isEndAnimation: false,
  executeFightAnimation: async () => {},
  showRanking: false,
  toggleRanking: () => {},
};
export const ComparerContext = createContext<ComparerContextType>(defaultValue);

const ComparerContextProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [people, setPeople] = useState(defaultValue.people);
  const [currentComparison, setCurrentComparison] = useState(
    defaultValue.currentComparison,
  );
  const [isEndAnimation, setIsEndAnimation] = useState(
    defaultValue.isEndAnimation,
  );
  const [showRanking, setShowRanking] = useState(false);

  const setNewComparison = useCallback(() => {
    const currentComparison: [Person, Person] = getNewComparison(people);
    while (currentComparison[0].name === currentComparison[1].name) {
      currentComparison[1] = people[Math.floor(Math.random() * people.length)];
    }
    setCurrentComparison(currentComparison);
  }, [people]);
  const executeFightAnimation = async () => {
    setIsEndAnimation(true);
    await new Promise((r) => setTimeout(r, 4000));
    setIsEndAnimation(false);
  };
  const resolveComparison = (args: {
    winner: Person;
    loser: Person;
    isTie: boolean;
  }) => {
    setPeople((prev) => resolveComparisonUtil({ people: prev, ...args }));
  };

  const ComparerContextValue: ComparerContextType = {
    people,
    currentComparison,
    isEndAnimation,
    executeFightAnimation,
    setCurrentComparison: setNewComparison,
    resolveComparison,
    showRanking,
    toggleRanking: () => setShowRanking((prevState) => !prevState),
  };
  return (
    <ComparerContext.Provider value={ComparerContextValue}>
      {children}
    </ComparerContext.Provider>
  );
};

export default ComparerContextProvider;
