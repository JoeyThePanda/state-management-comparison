"use client"
import { useContext, useState } from "react";
import styles from "@/app/comparer/page.module.css";
import { ComparerContext } from "@/app/comparer/context/ComparerContext";

export const useContextPersonCard = (side: "left" | "right") => {
  const {
    resolveComparison,
    setCurrentComparison,
    isEndAnimation,
    executeFightAnimation,
    currentComparison,
  } = useContext(ComparerContext);
  const [isShown, setIsShown] = useState(false);
  const [isWinner, setIsWinner] = useState(false);
  const getAnimationClass = () => {
    if (isWinner && isEndAnimation && side === "left") return styles.winnerLeft;
    if (isWinner && isEndAnimation && side === "right")
      return styles.winnerRight;
    if (!isWinner && isEndAnimation && side === "left") return styles.loserLeft;
    if (!isWinner && isEndAnimation && side === "right")
      return styles.loserRight;
  };
  const [person, opponent] =
    side === "left"
      ? currentComparison
      : [currentComparison[1], currentComparison[0]];
  const onWinningClick = async () => {
    resolveComparison({ winner: person, loser: opponent, isTie: false });
    setIsWinner(true);
    await executeFightAnimation();
    setIsWinner(false);
    setCurrentComparison();
  };

  return {
    isShown,
    setIsShown,
    person,
    onWinningClick,
    getAnimationClass,
  };
};