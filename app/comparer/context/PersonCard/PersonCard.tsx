"use client";
import { FC } from "react";
import Image from "next/image";
import styles from "../../page.module.css";
import { clsx } from "clsx";
import { useContextPersonCard } from "@/app/comparer/context/PersonCard/usePersonCardContext";

const PersonCard: FC<{
  side: "left" | "right";
}> = ({ side }) => {
  const { isShown, setIsShown, person, onWinningClick, getAnimationClass } =
    useContextPersonCard(side);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div
        onClick={onWinningClick}
        className={clsx(styles.clickable, getAnimationClass())}
      >
        <h2>{person.name}</h2>
        <p>Current Score {person.comparerScore}</p>
        {person.image && (
          <div className={styles.imageContainer}>
            <Image
              src={person.image}
              alt={person.name}
              fill
              style={{ objectFit: "contain" }}
              sizes={"100%"}
            />
          </div>
        )}
      </div>
      <button onClick={() => setIsShown(true)}>Mehr Info</button>
      {isShown && (
        <div
          className={clsx(
            side === "left" ? styles.infoCardLeft : styles.infoCardRight,
          )}
        >
          <p>{person.description}</p>
          <button onClick={() => setIsShown(false)}>X</button>
        </div>
      )}
    </div>
  );
};

export default PersonCard;
