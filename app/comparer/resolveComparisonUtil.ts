"use client"

import { Person } from "@/app/people";

const resolveComparisonUtil = ({
  winner,
  loser,
  isTie,
  people,
}: {
  winner: Person;
  loser: Person;
  isTie: boolean;
  people: Array<Person>;
}): Array<Person> => {
  const updatedPeople = people;
  const winnerIndex = updatedPeople.findIndex(
    (person) => person.name === winner.name,
  );
  const loserIndex = updatedPeople.findIndex(
    (person) => person.name === loser.name,
  );
  if (isTie) {
    const averageScore = (winner.comparerScore + loser.comparerScore) / 2;
    updatedPeople[winnerIndex].comparerScore = Math.ceil(averageScore);
    updatedPeople[loserIndex].comparerScore = Math.floor(averageScore);
  } else if (winner.comparerScore < loser.comparerScore) {
    const winnerScore = winner.comparerScore;
    updatedPeople[winnerIndex].comparerScore = loser.comparerScore;
    updatedPeople[loserIndex].comparerScore = winnerScore;
  } else if (winner.comparerScore === loser.comparerScore) {
    updatedPeople[winnerIndex].comparerScore++;
    updatedPeople[loserIndex].comparerScore--;
  }
  return updatedPeople;
};

export default resolveComparisonUtil;
