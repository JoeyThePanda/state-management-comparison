import peopleJson from "@/app/people.json"
import { StaticImageData } from "next/image";


export interface Person {
  name: string;
  description: string;
  image: StaticImageData | string;
  matchUps: Array<Person>;
  comparerScore: number;
}
export const PEOPLE: Array<Omit<Person, "matchUps" | "comparerScore">> = peopleJson
